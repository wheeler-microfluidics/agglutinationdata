# Annotating using this software pipeline

## Requirements and setup

- React, version 17 and above
    - You can download this at the following link: https://react-cn.github.io/react/downloads.html

<br/>
<br/>

## Setting up the Google API

To set up your Google Spreadsheets API project, I recommend following this video **from minute 8 to minute 20** only: https://www.youtube.com/watch?v=yhCJU4aqMb4

This will give you the following secrets:

    - API_KEY
    - CLIENT_ID

Note: in general, you should not have these secrets on your frontend (they should be on a server). However you can specify configurations for your google API project that make it safe to do so.
<br/>
<br/>

## config.js

This is where you can configure your specific application. In the config dictionary:

    "appTitle": The title of the application shown at the top of the page,
    "imageTitleMapper": "TODO: This will be a seperate column in the spreadsheet.",
    "adminEmail": The email of the admin (will show on error with logins),
    "sheetsConfig": {
        "API_KEY": The API key defined above,
        "CLIENT_ID": The client ID defined above,
        "SCOPE": The scope of the app
    },
    "users": {
        "SAMPLEUSER@gmail.com": {
                "SPREADSHEET_ID": The ID of the spreadsheet to be mutated,
                "SPREADSHEET_RANGE": The page in that spreadsheet,
                "URL_SUBSTRINGS_TO_ANNOTATE": A list of substrings to match for annotation (can be empty)
        },
        "default": default settings of the dictionary described above
    },
    "labels": The kind of labels you wish to have (buttons vs. text),
    "annotationRules": A dictionary of annotation rules which describe how things should be annotated.
<br/>
<br/>

