import { filelist } from '../../individual_well_imgs/img_files.js';

const getSlidesList = (files, wellName) => {
    var slidesList = [];
    for (var index = 0; index < files.length; index++) {
        var file = files[index];
        slidesList.push(
            <div>
                <div className="wellReferenceLabel">
                    <div className="wellName">
                        {wellName}
                    </div>
                    <div className="numbertext">{"" + index + "/" + files.length}</div>
                </div>
                <img src={file} style={{height: 150, width: 150}}/>
            </div>
        );
    }
    return slidesList;
}

var zero_files = filelist["0"];
var one_files = filelist["1"];
var two_files = filelist["2"];
var three_files = filelist["3"];
var four_files = filelist["4"];
var na_files = filelist["NA"];

export const zeroSlidesList = getSlidesList(zero_files, "0");
export const oneSlidesList = getSlidesList(one_files, "+1");
export const twoSlidesList = getSlidesList(two_files, "+2");
export const threeSlidesList = getSlidesList(three_files, "+3");
export const fourSlidesList = getSlidesList(four_files, "+4");
export const naSlidesList = getSlidesList(na_files, "N/A");
