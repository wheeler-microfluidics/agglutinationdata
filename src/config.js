const CONFIG = {
      "appTitle": "WheelerLab Agglutination Data Annotator",
      "imageTitleMapper": "TODO: This will be a seperate column in the spreadsheet.",
      "adminEmail": "sid.gupta@mail.utoronto.ca",
      "sheetsConfig": {
            "API_KEY": "AIzaSyDY63oaWE0SEcgPL2Hf1hSz8wWLwlYqd6Q",
            "CLIENT_ID": "509563213429-46knn18d8v6krn02vcn2merag004212d.apps.googleusercontent.com",
            "SCOPE": "https://www.googleapis.com/auth/spreadsheets"
      },
      "users": {
            "agglutinationtest@gmail.com": {
                  "SPREADSHEET_ID": "1UBH-A4dQcyGGN1kf3BgPVZDThV7US15MJ3f2udJf5ek",
                  "SPREADSHEET_RANGE": "model_mistakes_2",
                  "URL_SUBSTRINGS_TO_ANNOTATE": []
            },
            "default": {
                  "SPREADSHEET_ID": "1UBH-A4dQcyGGN1kf3BgPVZDThV7US15MJ3f2udJf5ek",
                  "SPREADSHEET_RANGE": "model_mistakes_2",
                  "URL_SUBSTRINGS_TO_ANNOTATE": []
            }
      },
      "labels": {
            "type": "buttons",
            "numLabels": 5,
            "labelNames": ["0", "+1", "+2", "+3", "+4", "N/A"],
            "labelValues": [0, 1, 2, 3, 4, -5],
            "referenceImages": {
                  "0": [],
                  "+1": [],
                  "+2": [],
                  "+3": [],
                  "+4": [],
                  "N/A": []
            }
      },
      "annotationRules": {
            "0 - No evidence of agglutination": [
                  "Nothing, or only cloudiness"
            ],
            "1 - Any evidence of agglutination": [
                  "Small specs",
                  "Cloudiness where the background breaks up into different regions (but there must be specs)"
            ],
            "2 - Evidence of agglutination": [
                  "Getting darker and more pronounced",
                  "Still have blobs the are scattered",
            ],
            "3 - Definite agglutination": [
                  "Textbook definition: a few large agglutinates",
                  "Scattered blobs look like they are starting to connect",
                  "A few large dark blobs. The more the spread, the more likely it is a 2",
                  "Dark blobs, but cloudiness in the background"
            ],
            "4 - Strong agglutination": [
                  "Strong dark blob",
                  "No cloudiness in the background",
                  "Use cloudiness in the background to distinguish between a 3 & 4"
            ]
      }
}
export default CONFIG